import React, { Component } from "react";

export default class TodoForm extends Component {
  render() {
    return (
      <div>
        <input
          type="text"
          autoFocus="autofocus"
          placeholder="Add todo here"
          onKeyPress={e => {
            if (e.key === "Enter") {
              if (e.target.value !== "") {
                this.props.val(e.target.value);
                e.target.value = "";
              }
            }
          }}
        />
        <input type="submit" value="changeThis" />
      </div>
    );
  }
}
