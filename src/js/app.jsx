import React, { Component } from "react";
import { render } from "react-dom";
import TodoBox from "./TodoBox";

export default class TodoContainer extends Component {
  render() {
    return <TodoBox />
  }
}

render(<TodoContainer />, document.getElementById("todoContainer"));
