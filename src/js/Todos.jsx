import React from "react";

export default class Todo extends React.Component {
  render() {
    let todo = this.props.data.map((obj, i) => (
      <li key={i}>
        <input
          type="checkbox"
          checked={obj.checked}
          onChange={() => this.props.checkHandler(i, obj)}
        />
        <span>{obj.text}</span>
        <input
          type="submit"
          value="x"
          onClick={() => this.props.delHandler(i, obj)}
        />
      </li>
    ));
    return <ul>{todo}</ul>;
  }
}
