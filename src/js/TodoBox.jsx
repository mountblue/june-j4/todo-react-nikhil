import React, { Component } from "react";
import TodoForm from "./TodoForm";
import Todo from "./Todos";

export default class TodoBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      incomingValue: []
    };
  }
  componentDidMount() {
    fetch("../api/todo")
      .then(data => data.json())
      .then(data => {
        this.setState({
          incomingValue: data
        });
      });
  }

  handleCheck(i, obj) {
    let arr = [...this.state.incomingValue];
    arr[i].checked = arr[i].checked ? false : true;
    this.setState({
      incomingValue: arr
    });
    fetch("../api/todo/" + obj._id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({checked: obj.checked})
    });
  }

  handleDelete(i, obj) {
    let arr = [...this.state.incomingValue];
    arr.splice(i, 1);
    this.setState({
      incomingValue: arr
    });
    fetch("../api/todo/" + obj._id, {
      method: "DELETE"
    });
  }

  render() {
    return (
      <div>
        <TodoForm
          val={text => {
            let obj = {
              checked: false,
              text,
              _id: Math.floor(Math.random() * Date.now())
            };
            this.setState({
              incomingValue: [...this.state.incomingValue, obj]
            });
            fetch("../api/todo/", {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(obj)
            });
          }}
        />
        <Todo
          data={this.state.incomingValue}
          checkHandler={(i, obj) => this.handleCheck(i, obj)}
          delHandler={(i, obj) => this.handleDelete(i, obj)}
        />
      </div>
    );
  }
}
