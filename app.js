const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Schema = mongoose.Schema

app.use(express.static('dist'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get('/', function (req, res) {
    res.render('index')
})

mongoose.connect('mongodb://localhost:27017/todo', {
    useNewUrlParser: true
})

let todoSchema = new Schema({
    _id: Number,
    checked: Boolean,
    text: String
}, {
    autoIndex: false
})

let TodoDB = mongoose.model('TodoDB', todoSchema)

app.get('/api/todo', function (req, res) {
    TodoDB.find({}, function (err, data) {
        res.send(data)
    })
})

app.post('/api/todo', function (req, res) {
    TodoDB.create(req.body, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.put('/api/todo/:id', function (req, res) {
    let _id = (req.params.id)
    let check = (req.body.checked)
    TodoDB.findOneAndUpdate({
        _id
    }, {
        $set: {
            checked: check
        }
    }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.delete('/api/todo/:id', function (req, res) {
    let _id = (req.params.id)
    TodoDB.deleteOne({
        _id
    }, function (err, data) {
        if (err) {
            console.log(err)
        } else {
            res.send()
        }
    })
})

app.listen(3000, function () {
    console.log('listening on 3000!')
})